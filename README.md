# Sample OAuth 2.0 authorization server & client implementations

Language: [golang](https://golang.org/)

Client library: [golang.org/x/oauth2](https://golang.org/x/oauth2)

Server libraries:

1. [github.com/go-oauth2/oauth2](https://github.com/go-oauth2/oauth2)
1. [github.com/ory/fosite](https://github.com/ory/fosite)

# Building

Execute `make` to generate executable binaries under "build/".

## References

Common:

1. [IETF| OAuth 2.0 for Browser-Based Apps](https://tools.ietf.org/id/draft-parecki-oauth-browser-based-apps-02.txt)
1. [IETF| RFC 6749 - OAuth 2.0 Authorization Framework](https://tools.ietf.org/html/rfc6749)
1. [IETF| RFC 6819 - OAuth 2.0 Threat Model…](https://tools.ietf.org/html/rfc6819)
1. [IETF| RFC 7636 - OAuth Proof Key for Code Exchange…](https://tools.ietf.org/html/rfc7636)
1. [OAuth libraries for Go](https://oauth.net/code/go/)
1. [Protecting Mobile Apps with PKCE](https://www.oauth.com/oauth2-servers/pkce/)
1. [oauth.net](https://oauth.net/2/)

## Entities

**The token & client tables are created by the `github.com/go-oauth2/oauth2`
authorization server manager if they don't exist.**

Token (`oauth2_tokens`):

- id: bigserial
- created_at: timestampz
- expires_at: timestampz
- code: text
- access: text
- refresh: text
- data: jsonb

Client entity (`oauth2_clients`):

- id: text;
- secret: text;
- domain: text; and
- data: jsonb

User:

- Username: text,
- Password: text (password hash),
- Role: []text

## Misc

Configs are stored in the environment.

# WIP

The [github.com/ory/fosite](https://github.com/ory/fosite) server
implementation is not complete.
