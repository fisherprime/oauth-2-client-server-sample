// Package main provides an OAuth 2.0 authorization server binary implemented
// with go-oauth2/oauth2.
package main

import (
	"flag"
	"fmt"
	"os"

	"github.com/ilyakaznacheev/cleanenv"
	"gitlab.com/fisherprime/oauth-2-client-server-sample/internal/server"

	log "github.com/sirupsen/logrus"
	"gopkg.in/yaml.v3"
)

func main() {
	const configExample = "config-sample.yml"

	var (
		args struct {
			ConfigFile         string
			GenerateConfigFile bool
		}
		cfg server.Config
	)

	flags := flag.NewFlagSet("OAuth 2.0 server", flag.ExitOnError)
	flags.StringVar(&args.ConfigFile, "c", "configs/server/config.yml", "Path to configuration file")
	flags.BoolVar(&args.GenerateConfigFile, "g", false, fmt.Sprintf("Generate sample configuration file: %s", configExample))

	flagsUsage := flags.Usage
	flags.Usage = func() {
		flagsUsage()

		if descr, _ := cleanenv.GetDescription(&cfg, nil); descr != "" {
			fmt.Fprintln(flags.Output(), "\n", descr)
		}
	}

	if err := flags.Parse(os.Args[1:]); err != nil {
		log.Fatal(err)
	}

	if args.GenerateConfigFile {
		output, err := yaml.Marshal(server.NewDefaultConfig())
		if err != nil {
			log.Fatal(err)
		}

		if err := server.OverwriteFile(configExample, output); err != nil {
			log.Fatal(err)
		}

		return
	}

	if err := cleanenv.ReadConfig(args.ConfigFile, &cfg); err != nil {
		log.Fatal(err)
	}

	log.Fatal(server.StartServer(&cfg))
}
