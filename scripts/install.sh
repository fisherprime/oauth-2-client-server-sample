#!/bin/bash

BASE_DIR="$(
	# cd -P .. || exit
	pwd
)"

ldflags="-X 'main.Version=$1' -X 'main.Build=${*:2:2}'"

_install() {
	install_target="$(basename "$1")"

	printf "  > Installing: %s\n" "$install_target"
	go install -ldflags="$ldflags" -trimpath "$1"
}

if [[ $# -eq 2 ]]; then
	dir="$BASE_DIR/cmd/$1"
	[[ -d "$dir" ]] && _install "$dir"
else
	for dir in "$BASE_DIR"/cmd/*; do
		_install "$dir" &
	done
	wait
fi
