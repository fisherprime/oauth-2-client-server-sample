<a name=""></a>
##  (2021-03-29)


#### Bug Fixes

* ***:**
  *  Resolve issue with `generateRandString` ([fd314331](fd314331))
  *  Resolve nil pointer dereference ([3764f03b](3764f03b))
  *  Rework client & server grant implementations ([822f63ef](822f63ef))
  *  Rename the project ([68a1fa41](68a1fa41))
* ***gitignore:**  Move gitignore to .gitignore ([ade8414d](ade8414d))
* **cmd/*client:**  Alter the internal imports ([989418cf](989418cf))
* **internal/client:**  Change to a random length `state` ([baebe42b](baebe42b))
* **internal/client/utils.go:**  Change the base64 encoder ([9cdcfa2b](9cdcfa2b))
* **internal/server/configure.go:**  Fix error mapping the stores ([9e3a844b](9e3a844b))
* **internal/v1/client/*:**  Resolve linter warnings ([f49a97d7](f49a97d7))

#### Performance

* ***:**
  *  Rework config example generation ([94b36406](94b36406))
  *  Overhaul code ([5b6dbea4](5b6dbea4))
  *  Change env parser to cleanenv ([14731be0](14731be0))
* **internal/v1/*:**  Add `http.Server` graceful termination ([ab511a5c](ab511a5c))

#### Features

* ***:**  Add initial OAuth 2.0 examples ([dd9656df](dd9656df))
* ***/server2*:**  Add fosite authorization server code ([222337fb](222337fb))
* **internal/server/configure.go:**  Add server configuration options ([693f3ca9](693f3ca9))
* **internal/server/v1/handlers.go:**  Add `scope` output to the testHandler ([e208c68c](e208c68c))



<a name=""></a>
##  (2021-03-28)


#### Features

* ***:**  Add initial OAuth 2.0 examples ([dd9656df](dd9656df))
* ***/server2*:**  Add fosite authorization server code ([222337fb](222337fb))
* **internal/server/configure.go:**  Add server configuration options ([693f3ca9](693f3ca9))
* **internal/server/v1/handlers.go:**  Add `scope` output to the testHandler ([e208c68c](e208c68c))

#### Performance

* ***:**
  *  Rework config example generation ([94b36406](94b36406))
  *  Overhaul code ([5b6dbea4](5b6dbea4))
  *  Change env parser to cleanenv ([14731be0](14731be0))
* **internal/v1/*:**  Add `http.Server` graceful termination ([ab511a5c](ab511a5c))

#### Bug Fixes

* ***:**
  *  Resolve issue with `generateRandString` ([fd314331](fd314331))
  *  Resolve nil pointer dereference ([3764f03b](3764f03b))
  *  Rework client & server grant implementations ([822f63ef](822f63ef))
  *  Rename the project ([68a1fa41](68a1fa41))
* ***gitignore:**  Move gitignore to .gitignore ([ade8414d](ade8414d))
* **cmd/*client:**  Alter the internal imports ([989418cf](989418cf))
* **internal/client:**  Change to a random length `state` ([baebe42b](baebe42b))
* **internal/client/utils.go:**  Change the base64 encoder ([9cdcfa2b](9cdcfa2b))
* **internal/server/configure.go:**  Fix error mapping the stores ([9e3a844b](9e3a844b))
* **internal/v1/client/*:**  Resolve linter warnings ([f49a97d7](f49a97d7))



<a name=""></a>
##  (2021-03-27)


#### Performance

* ***:**
  *  Rework config example generation ([94b36406](94b36406))
  *  Overhaul code ([5b6dbea4](5b6dbea4))
  *  Change env parser to cleanenv ([14731be0](14731be0))
* **internal/v1/*:**  Add `http.Server` graceful termination ([ab511a5c](ab511a5c))

#### Bug Fixes

* ***:**
  *  Resolve issue with `generateRandString` ([fd314331](fd314331))
  *  Resolve nil pointer dereference ([3764f03b](3764f03b))
  *  Rework client & server grant implementations ([822f63ef](822f63ef))
  *  Rename the project ([68a1fa41](68a1fa41))
* ***gitignore:**  Move gitignore to .gitignore ([ade8414d](ade8414d))
* **cmd/*client:**  Alter the internal imports ([989418cf](989418cf))
* **internal/client:**  Change to a random length `state` ([baebe42b](baebe42b))
* **internal/client/utils.go:**  Change the base64 encoder ([9cdcfa2b](9cdcfa2b))
* **internal/server/configure.go:**  Fix error mapping the stores ([9e3a844b](9e3a844b))
* **internal/v1/client/*:**  Resolve linter warnings ([f49a97d7](f49a97d7))

#### Features

* ***:**  Add initial OAuth 2.0 examples ([dd9656df](dd9656df))
* ***/server2*:**  Add fosite authorization server code ([222337fb](222337fb))
* **internal/server/configure.go:**  Add server configuration options ([693f3ca9](693f3ca9))
* **internal/server/v1/handlers.go:**  Add `scope` output to the testHandler ([e208c68c](e208c68c))



<a name=""></a>
##  (2021-03-27)


#### Features

* ***:**  Add initial OAuth 2.0 examples ([dd9656df](dd9656df))
* ***/server2*:**  Add fosite authorization server code ([222337fb](222337fb))
* **internal/server/configure.go:**  Add server configuration options ([693f3ca9](693f3ca9))
* **internal/server/v1/handlers.go:**  Add `scope` output to the testHandler ([e208c68c](e208c68c))

#### Performance

* ***:**
  *  Rework config example generation ([94b36406](94b36406))
  *  Overhaul code ([5b6dbea4](5b6dbea4))
  *  Change env parser to cleanenv ([14731be0](14731be0))
* **internal/v1/*:**  Add `http.Server` graceful termination ([ab511a5c](ab511a5c))

#### Bug Fixes

* ***:**
  *  Resolve issue with `generateRandString` ([fd314331](fd314331))
  *  Resolve nil pointer dereference ([3764f03b](3764f03b))
  *  Rework client & server grant implementations ([822f63ef](822f63ef))
  *  Rename the project ([68a1fa41](68a1fa41))
* ***gitignore:**  Move gitignore to .gitignore ([ade8414d](ade8414d))
* **cmd/*client:**  Alter the internal imports ([989418cf](989418cf))
* **internal/client:**  Change to a random length `state` ([baebe42b](baebe42b))
* **internal/client/utils.go:**  Change the base64 encoder ([9cdcfa2b](9cdcfa2b))
* **internal/server/configure.go:**  Fix error mapping the stores ([9e3a844b](9e3a844b))
* **internal/v1/client/*:**  Resolve linter warnings ([f49a97d7](f49a97d7))



<a name=""></a>
##  (2021-03-27)


#### Performance

* ***:**
  *  Rework config example generation ([94b36406](94b36406))
  *  Overhaul code ([5b6dbea4](5b6dbea4))
  *  Change env parser to cleanenv ([14731be0](14731be0))
* **internal/v1/*:**  Add `http.Server` graceful termination ([ab511a5c](ab511a5c))

#### Features

* ***:**  Add initial OAuth 2.0 examples ([dd9656df](dd9656df))
* ***/server2*:**  Add fosite authorization server code ([222337fb](222337fb))
* **internal/server/configure.go:**  Add server configuration options ([693f3ca9](693f3ca9))
* **internal/server/v1/handlers.go:**  Add `scope` output to the testHandler ([e208c68c](e208c68c))

#### Bug Fixes

* ***:**
  *  Resolve issue with `generateRandString` ([fd314331](fd314331))
  *  Resolve nil pointer dereference ([3764f03b](3764f03b))
  *  Rework client & server grant implementations ([822f63ef](822f63ef))
  *  Rename the project ([68a1fa41](68a1fa41))
* ***gitignore:**  Move gitignore to .gitignore ([ade8414d](ade8414d))
* **cmd/*client:**  Alter the internal imports ([989418cf](989418cf))
* **internal/client:**  Change to a random length `state` ([baebe42b](baebe42b))
* **internal/client/utils.go:**  Change the base64 encoder ([9cdcfa2b](9cdcfa2b))
* **internal/server/configure.go:**  Fix error mapping the stores ([9e3a844b](9e3a844b))
* **internal/v1/client/*:**  Resolve linter warnings ([f49a97d7](f49a97d7))



<a name=""></a>
##  (2021-03-26)


#### Features

* ***:**  Add initial OAuth 2.0 examples ([dd9656df](dd9656df))
* ***/server2*:**  Add fosite authorization server code ([222337fb](222337fb))
* **internal/server/configure.go:**  Add server configuration options ([693f3ca9](693f3ca9))
* **internal/server/v1/handlers.go:**  Add `scope` output to the testHandler ([e208c68c](e208c68c))

#### Bug Fixes

* ***:**
  *  Resolve issue with `generateRandString` ([fd314331](fd314331))
  *  Resolve nil pointer dereference ([3764f03b](3764f03b))
  *  Rework client & server grant implementations ([822f63ef](822f63ef))
  *  Rename the project ([68a1fa41](68a1fa41))
* ***gitignore:**  Move gitignore to .gitignore ([ade8414d](ade8414d))
* **cmd/*client:**  Alter the internal imports ([989418cf](989418cf))
* **internal/client:**  Change to a random length `state` ([baebe42b](baebe42b))
* **internal/client/utils.go:**  Change the base64 encoder ([9cdcfa2b](9cdcfa2b))
* **internal/server/configure.go:**  Fix error mapping the stores ([9e3a844b](9e3a844b))

#### Performance

* ***:**
  *  Rework config example generation ([94b36406](94b36406))
  *  Overhaul code ([5b6dbea4](5b6dbea4))
  *  Change env parser to cleanenv ([14731be0](14731be0))



<a name=""></a>
##  (2021-03-20)


#### Bug Fixes

* ***:**
  *  Resolve nil pointer dereference ([3764f03b](3764f03b))
  *  Rework client & server grant implementations ([822f63ef](822f63ef))
  *  Rename the project ([68a1fa41](68a1fa41))
* ***gitignore:**  Move gitignore to .gitignore ([ade8414d](ade8414d))
* **cmd/*client:**  Alter the internal imports ([989418cf](989418cf))
* **internal/client:**  Change to a random length `state` ([baebe42b](baebe42b))
* **internal/client/utils.go:**  Change the base64 encoder ([9cdcfa2b](9cdcfa2b))
* **internal/server/configure.go:**  Fix error mapping the stores ([9e3a844b](9e3a844b))

#### Performance

* ***:**
  *  Overhaul code ([5b6dbea4](5b6dbea4))
  *  Change env parser to cleanenv ([14731be0](14731be0))

#### Features

* ***:**  Add initial OAuth 2.0 examples ([dd9656df](dd9656df))
* ***/server2*:**  Add fosite authorization server code ([222337fb](222337fb))
* **internal/server/configure.go:**  Add server configuration options ([693f3ca9](693f3ca9))
* **internal/server/v1/handlers.go:**  Add `scope` output to the testHandler ([e208c68c](e208c68c))



<a name=""></a>
##  (2021-03-19)


#### Features

* ***:**  Add initial OAuth 2.0 examples ([dd9656df](dd9656df))
* ***/server2*:**  Add fosite authorization server code ([222337fb](222337fb))
* **internal/server/configure.go:**  Add server configuration options ([693f3ca9](693f3ca9))
* **internal/server/v1/handlers.go:**  Add `scope` output to the testHandler ([e208c68c](e208c68c))

#### Performance

* ***:**
  *  Overhaul code ([5b6dbea4](5b6dbea4))
  *  Change env parser to cleanenv ([14731be0](14731be0))

#### Bug Fixes

* ***:**
  *  Resolve nil pointer dereference ([3764f03b](3764f03b))
  *  Rework client & server grant implementations ([822f63ef](822f63ef))
  *  Rename the project ([68a1fa41](68a1fa41))
* ***gitignore:**  Move gitignore to .gitignore ([ade8414d](ade8414d))
* **cmd/*client:**  Alter the internal imports ([989418cf](989418cf))
* **internal/client:**  Change to a random length `state` ([baebe42b](baebe42b))
* **internal/client/utils.go:**  Change the base64 encoder ([9cdcfa2b](9cdcfa2b))
* **internal/server/configure.go:**  Fix error mapping the stores ([9e3a844b](9e3a844b))



<a name=""></a>
##  (2021-03-19)


#### Features

* ***:**  Add initial OAuth 2.0 examples ([dd9656df](dd9656df))
* ***/server2*:**  Add fosite authorization server code ([222337fb](222337fb))
* **internal/server/configure.go:**  Add server configuration options ([693f3ca9](693f3ca9))
* **internal/server/v1/handlers.go:**  Add `scope` output to the testHandler ([e208c68c](e208c68c))

#### Performance

* ***:**
  *  Overhaul code ([5b6dbea4](5b6dbea4))
  *  Change env parser to cleanenv ([14731be0](14731be0))

#### Bug Fixes

* ***:**
  *  Resolve nil pointer dereference ([3764f03b](3764f03b))
  *  Rework client & server grant implementations ([822f63ef](822f63ef))
  *  Rename the project ([68a1fa41](68a1fa41))
* ***gitignore:**  Move gitignore to .gitignore ([ade8414d](ade8414d))
* **cmd/*client:**  Alter the internal imports ([989418cf](989418cf))
* **internal/client:**  Change to a random length `state` ([baebe42b](baebe42b))
* **internal/client/utils.go:**  Change the base64 encoder ([9cdcfa2b](9cdcfa2b))
* **internal/server/configure.go:**  Fix error mapping the stores ([9e3a844b](9e3a844b))



<a name=""></a>
##  (2021-03-18)


#### Bug Fixes

* ***:**
  *  Rework client & server grant implementations ([822f63ef](822f63ef))
  *  Rename the project ([68a1fa41](68a1fa41))
* ***gitignore:**  Move gitignore to .gitignore ([ade8414d](ade8414d))
* **cmd/*client:**  Alter the internal imports ([989418cf](989418cf))
* **internal/client:**  Change to a random length `state` ([baebe42b](baebe42b))
* **internal/client/utils.go:**  Change the base64 encoder ([9cdcfa2b](9cdcfa2b))
* **internal/server/configure.go:**  Fix error mapping the stores ([9e3a844b](9e3a844b))

#### Features

* ***:**  Add initial OAuth 2.0 examples ([dd9656df](dd9656df))
* ***/server2*:**  Add fosite authorization server code ([222337fb](222337fb))
* **internal/server/configure.go:**  Add server configuration options ([693f3ca9](693f3ca9))
* **internal/server/v1/handlers.go:**  Add `scope` output to the testHandler ([e208c68c](e208c68c))

#### Performance

* ***:**
  *  Overhaul code ([5b6dbea4](5b6dbea4))
  *  Change env parser to cleanenv ([14731be0](14731be0))



<a name=""></a>
##  (2021-03-08)


#### Bug Fixes

* ***:**
  *  Rework client & server grant implementations ([822f63ef](822f63ef))
  *  Rename the project ([68a1fa41](68a1fa41))
* ***gitignore:**  Move gitignore to .gitignore ([ade8414d](ade8414d))
* **cmd/*client:**  Alter the internal imports ([989418cf](989418cf))
* **internal/client:**  Change to a random length `state` ([baebe42b](baebe42b))
* **internal/client/utils.go:**  Change the base64 encoder ([9cdcfa2b](9cdcfa2b))
* **internal/server/configure.go:**  Fix error mapping the stores ([9e3a844b](9e3a844b))

#### Performance

* ***:**  Change env parser to cleanenv ([14731be0](14731be0))

#### Features

* ***:**  Add initial OAuth 2.0 examples ([dd9656df](dd9656df))
* ***/server2*:**  Add fosite authorization server code ([222337fb](222337fb))
* **internal/server/configure.go:**  Add server configuration options ([693f3ca9](693f3ca9))
* **internal/server/v1/handlers.go:**  Add `scope` output to the testHandler ([e208c68c](e208c68c))



<a name=""></a>
##  (2021-03-03)


#### Bug Fixes

* ***:**
  *  Rework client & server grant implementations ([822f63ef](822f63ef))
  *  Rename the project ([68a1fa41](68a1fa41))
* ***gitignore:**  Move gitignore to .gitignore ([ade8414d](ade8414d))
* **cmd/*client:**  Alter the internal imports ([989418cf](989418cf))
* **internal/client:**  Change to a random length `state` ([baebe42b](baebe42b))
* **internal/client/utils.go:**  Change the base64 encoder ([9cdcfa2b](9cdcfa2b))
* **internal/server/configure.go:**  Fix error mapping the stores ([9e3a844b](9e3a844b))

#### Features

* ***:**  Add initial OAuth 2.0 examples ([dd9656df](dd9656df))
* ***/server2*:**  Add fosite authorization server code ([222337fb](222337fb))
* **internal/server/configure.go:**  Add server configuration options ([693f3ca9](693f3ca9))
* **internal/server/v1/handlers.go:**  Add `scope` output to the testHandler ([e208c68c](e208c68c))



<a name=""></a>
##  (2021-02-23)


#### Bug Fixes

* ***:**
  *  Rework client & server grant implementations ([822f63ef](822f63ef))
  *  Rename the project ([68a1fa41](68a1fa41))
* ***gitignore:**  Move gitignore to .gitignore ([ade8414d](ade8414d))
* **cmd/*client:**  Alter the internal imports ([989418cf](989418cf))
* **internal/client:**  Change to a random length `state` ([baebe42b](baebe42b))
* **internal/client/utils.go:**  Change the base64 encoder ([9cdcfa2b](9cdcfa2b))
* **internal/server/configure.go:**  Fix error mapping the stores ([9e3a844b](9e3a844b))

#### Features

* ***:**  Add initial OAuth 2.0 examples ([dd9656df](dd9656df))
* ***/server2*:**  Add fosite authorization server code ([222337fb](222337fb))
* **internal/server/configure.go:**  Add server configuration options ([693f3ca9](693f3ca9))
* **internal/server/v1/handlers.go:**  Add `scope` output to the testHandler ([e208c68c](e208c68c))
