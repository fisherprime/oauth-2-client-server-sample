package server

import (
	"crypto/tls"
	"fmt"
	"net/http"
	"os"
	"path/filepath"

	"github.com/go-chi/chi"
)

// configureHTTPService creates a `http.Server` instance with optional TLS v1.3 security.
func configureHTTPService(router *chi.Mux) (srv *http.Server, err error) {
	var tlsCfg *tls.Config

	if cfg.TLS.CertfilePath != "" && cfg.TLS.KeyfilePath != "" {
		var (
			certPath string
			keyPath  string
			cert     tls.Certificate
		)

		certPath, err = filepath.Abs(cfg.TLS.CertfilePath)
		if err != nil {
			err = fmt.Errorf("could not obtain absolute TLS certfile path:  %v", err)
			return
		}
		keyPath, err = filepath.Abs(cfg.TLS.KeyfilePath)
		if err != nil {
			err = fmt.Errorf("could not obtain absolute TLS keyfile path:  %v", err)
			return
		}

		cert, err = tls.LoadX509KeyPair(certPath, keyPath)
		if err != nil {
			err = fmt.Errorf("could not load TLS certfile & keyfile:  %v", err)
			return
		}

		tlsCfg = &tls.Config{
			Certificates: []tls.Certificate{cert},
			MinVersion:   tls.VersionTLS13,
		}
	}

	srv = &http.Server{
		Addr:         fmt.Sprintf("%s:%d", cfg.Runtime.ServiceHost, cfg.Runtime.ServicePort),
		Handler:      router,
		ReadTimeout:  cfg.Runtime.RequestReadTimeout,
		WriteTimeout: cfg.Runtime.ResponseWriteTimeout,
		TLSConfig:    tlsCfg,
	}

	return
}

// OverwriteFile overwrites the contents of a file with the supplied data.
func OverwriteFile(path string, data []byte) (err error) {
	var (
		outputPath string
		file       *os.File
	)

	outputPath, err = filepath.Abs(path)
	if err != nil {
		return
	}

	file, err = os.OpenFile(outputPath, os.O_CREATE|os.O_RDWR, 0600)
	if err != nil {
		err = fmt.Errorf("failed to open file %s: %v", outputPath, err)
		return
	}

	_, err = file.Write(data)

	return
}
