package server

import "time"

// Config defines the neccessary configuration options for the implemented OAuth 2.0 server.
type Config struct {
	Runtime struct {
		AuthCodeExp              time.Duration `yaml:"auth_code_exp"`
		AccessTokenExp           time.Duration `yaml:"access_token_exp"`
		AuthCodeRefreshTokenExp  time.Duration `yaml:"auth_code_refresh_token_exp"`
		PassCredsRefreshTokenExp time.Duration `yaml:"pass_creds_refresh_token_exp"`

		DatabaseURL string `yaml:"database_url"`
		ServiceHost string `yaml:"service_host"`
		ServicePort int    `yaml:"service_port"`

		RequestReadTimeout   time.Duration `yaml:"request_read_timeout"`
		ResponseWriteTimeout time.Duration `yaml:"response_write_timeout"`
	} `yaml:"runtime"`

	TLS struct {
		CertfilePath string `yaml:"certfile_path"`
		KeyfilePath  string `yaml:"keyfile_path"`
	} `yaml:"tls"`
}

// NewDefaultConfig generates the default `Config` for the OAuth 2.0 server.
//
// nolint: funlen
func NewDefaultConfig() *Config {
	authCodeExp, _ := time.ParseDuration("60s")
	accessExp, _ := time.ParseDuration("1h")
	refreshExp, _ := time.ParseDuration("48h")
	readTimeout, _ := time.ParseDuration("5s")
	respTimeout, _ := time.ParseDuration("10s")

	return &Config{
		Runtime: struct {
			AuthCodeExp              time.Duration "yaml:\"auth_code_exp\""
			AccessTokenExp           time.Duration "yaml:\"access_token_exp\""
			AuthCodeRefreshTokenExp  time.Duration "yaml:\"auth_code_refresh_token_exp\""
			PassCredsRefreshTokenExp time.Duration "yaml:\"pass_creds_refresh_token_exp\""
			DatabaseURL              string        "yaml:\"database_url\""
			ServiceHost              string        "yaml:\"service_host\""
			ServicePort              int           "yaml:\"service_port\""
			RequestReadTimeout       time.Duration "yaml:\"request_read_timeout\""
			ResponseWriteTimeout     time.Duration "yaml:\"response_write_timeout\""
		}{
			AuthCodeExp:              authCodeExp,
			AccessTokenExp:           accessExp,
			AuthCodeRefreshTokenExp:  refreshExp,
			PassCredsRefreshTokenExp: refreshExp,
			DatabaseURL:              "",
			ServiceHost:              "127.0.0.1",
			ServicePort:              8019,
			RequestReadTimeout:       readTimeout,
			ResponseWriteTimeout:     respTimeout,
		},
		TLS: struct {
			CertfilePath string "yaml:\"certfile_path\""
			KeyfilePath  string "yaml:\"keyfile_path\""
		}{
			CertfilePath: "",
			KeyfilePath:  "",
		},
	}
}
