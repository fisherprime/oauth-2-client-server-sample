package client

import "time"

// Config defines the necessary configuration options for the implemented OAuth 2.0 client.
type Config struct {
	Endpoint struct {
		Test  string `yaml:"test"`
		Token string `yaml:"token"`

		Auth string `yaml:"auth"`
	} `yaml:"endpoint"`

	Secret struct {
		ClientID          string   `yaml:"client_id"`
		ClientSecret      string   `yaml:"client_secret"`
		OwnerClientID     string   `yaml:"owner_client_id"`
		OwnerClientSecret string   `yaml:"owner_client_secret"`
		OwnerPassword     string   `yaml:"owner_password"`
		OwnerUsername     string   `yaml:"owner_username"`
		Scopes            []string `yaml:"scopes"`
	} `yaml:"secret"`

	Runtime struct {
		RedirectURL string `yaml:"redirect_url"`
		LoginURL    string `yaml:"login_url"`
		ServiceHost string `yaml:"service_host"`
		ServicePort int    `yaml:"service_port"`

		RequestReadTimeout   time.Duration `yaml:"request_read_timeout"`
		ResponseWriteTimeout time.Duration `yaml:"response_write_timeout"`
	} `yaml:"runtime"`

	TLS struct {
		CertfilePath string `yaml:"certfile_path"`
		KeyfilePath  string `yaml:"keyfile_path"`
	} `yaml:"tls"`
}

// NewDefaultConfig generates the default `Config` for the OAuth 2.0 client.
func NewDefaultConfig() *Config {
	reqTimeout, _ := time.ParseDuration("5s")
	respTimeout, _ := time.ParseDuration("10s")

	return &Config{
		Endpoint: struct {
			Test  string "yaml:\"test\""
			Token string "yaml:\"token\""
			Auth  string "yaml:\"auth\""
		}{
			Test:  "/test",
			Token: "/token",
			Auth:  "/auth",
		},
		Secret: struct {
			ClientID          string   "yaml:\"client_id\""
			ClientSecret      string   "yaml:\"client_secret\""
			OwnerClientID     string   "yaml:\"owner_client_id\""
			OwnerClientSecret string   "yaml:\"owner_client_secret\""
			OwnerPassword     string   "yaml:\"owner_password\""
			OwnerUsername     string   "yaml:\"owner_username\""
			Scopes            []string "yaml:\"scopes\""
		}{
			ClientID:          "",
			ClientSecret:      "",
			OwnerClientID:     "",
			OwnerClientSecret: "",
			OwnerPassword:     "",
			OwnerUsername:     "",
			Scopes:            []string{},
		},
		Runtime: struct {
			RedirectURL          string        "yaml:\"redirect_url\""
			LoginURL             string        "yaml:\"login_url\""
			ServiceHost          string        "yaml:\"service_host\""
			ServicePort          int           "yaml:\"service_port\""
			RequestReadTimeout   time.Duration "yaml:\"request_read_timeout\""
			ResponseWriteTimeout time.Duration "yaml:\"response_write_timeout\""
		}{
			RedirectURL:          "/",
			LoginURL:             "/login",
			ServiceHost:          "127.0.0.1",
			ServicePort:          8018,
			RequestReadTimeout:   reqTimeout,
			ResponseWriteTimeout: respTimeout,
		},
		TLS: struct {
			CertfilePath string "yaml:\"certfile_path\""
			KeyfilePath  string "yaml:\"keyfile_path\""
		}{
			CertfilePath: "",
			KeyfilePath:  "",
		},
	}
}
