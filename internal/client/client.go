// Package client implements sample OAuth 2.0 clients.
//
// The implemented clients use std(in/out) & HTTP for the following grants: Authorization Code,
// Client Credentials & Resource Owner Password Credentials.
//
// Implemented with https://golang.org/x/oauth2 & https://golang.org/x/oauth2/clientcredentials
//
// REF: https://github.com/go-oauth2/oauth2/blob/v4.2.0/example/client/client.go.
package client

import (
	"context"
	"math/rand"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	log "github.com/sirupsen/logrus"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/clientcredentials"
)

var (
	cfg Config

	// clientConf contains the configuration for a client that is not the resource owner.
	clientConf *oauth2.Config

	// resourceOwnerConf contains the configuration for a client that is the resource owner.
	resourceOwnerConf *clientcredentials.Config
)

// StartCLIClient initiates the execution of the CLI OAuth 2.0 grant implementations.
func StartCLIClient(c *Config) {
	cfg = *c

	rand.Seed(time.Now().UnixNano())

	// clientConf contains the configuration for a client that is not the resource owner.
	clientConf = &oauth2.Config{
		ClientID:     cfg.Secret.ClientID,
		ClientSecret: cfg.Secret.ClientSecret,
		Scopes:       cfg.Secret.Scopes,
		RedirectURL:  cfg.Runtime.RedirectURL,
		Endpoint: oauth2.Endpoint{
			AuthURL:  cfg.Endpoint.Auth,
			TokenURL: cfg.Endpoint.Token,
		},
	}

	// resourceOwnerConf contains the configuration for a client that is the resource owner.
	resourceOwnerConf = &clientcredentials.Config{
		ClientID:     cfg.Secret.OwnerClientID,
		ClientSecret: cfg.Secret.OwnerClientSecret,
		Scopes:       cfg.Secret.Scopes,
		TokenURL:     cfg.Endpoint.Token,
	}

	// Two-legged authorization.
	if err := clientCredsGrantCLI(); err != nil {
		log.Error(err.Error())
	}
	if err := resourceOwnerPassCredsGrantCLI(); err != nil {
		log.Error(err.Error())
	}

	// Three-legged authorization
	if err := authCodeGrantCLI(); err != nil {
		log.Error(err.Error())
	}
}

// StartHTTPClient initiates the execution of the HTTP service OAuth 2.0 grant implementations.
//
// An error is returned on failure to configure the HTTP service.
func StartHTTPClient(c *Config) (err error) {
	cfg = *c

	rand.Seed(time.Now().UnixNano())

	// clientConf contains the configuration for a client that is not the resource owner.
	clientConf = &oauth2.Config{
		ClientID:     cfg.Secret.ClientID,
		ClientSecret: cfg.Secret.ClientSecret,
		Scopes:       cfg.Secret.Scopes,
		RedirectURL:  cfg.Runtime.RedirectURL,
		Endpoint: oauth2.Endpoint{
			AuthURL:  cfg.Endpoint.Auth,
			TokenURL: cfg.Endpoint.Token,
		},
	}

	// resourceOwnerConf contains the configuration for a client that is the resource owner.
	resourceOwnerConf = &clientcredentials.Config{
		ClientID:     cfg.Secret.OwnerClientID,
		ClientSecret: cfg.Secret.OwnerClientSecret,
		Scopes:       cfg.Secret.Scopes,
		TokenURL:     cfg.Endpoint.Token,
	}

	router := chi.NewRouter()
	router.Use(middleware.Logger, middleware.Recoverer)

	// Three-legged authorization.
	router.Get("/", authCodeGrantObtainAuthCode)
	router.Get(cfg.Runtime.RedirectURL, authCodeGrantObtainToken)

	// Two-legged authorization.
	router.Post(cfg.Runtime.LoginURL, loginHandler)

	srv, err := configureHTTPService(router)
	if err != nil {
		return
	}

	idleConnsClosed := make(chan struct{})
	shutdownService(idleConnsClosed, srv)

	if srv.TLSConfig != nil {
		err = srv.ListenAndServeTLS("", "")
	} else {
		err = srv.ListenAndServe()
	}
	if err != http.ErrServerClosed {
		return
	}
	err = nil

	<-idleConnsClosed

	return
}

// shutdownService allows for graceful shutdown of the HTTP service.
func shutdownService(idleConnsClosed chan struct{}, srv *http.Server) {
	sigint := make(chan os.Signal, 1)
	signal.Notify(sigint, os.Interrupt)
	<-sigint

	// We received an interrupt signal, shut down.
	if err := srv.Shutdown(context.Background()); err != nil {
		// Error from closing listeners, or context timeout:
		log.Printf("HTTP service Shutdown: %v", err)
	}
	close(idleConnsClosed)
}
