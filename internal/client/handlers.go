package client

import (
	"context"
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"

	"golang.org/x/oauth2"
)

var (
	// clientStateLimits defines the lower & upper length limits for a client `state`.
	clientStateLimits = [2]int{32, 64}

	// StateLen is the length for the "state" request parameter used in preventing CSRF.
	StateLen = clientStateLimits[0] + rand.Intn(clientStateLimits[1]) // nolint: gosec

	// ctx is a generic context.
	ctx = context.Background()

	// clientState is a random string passed to the authorization server & validated by the client;
	// used to mitigate CSRF attacks.
	clientState string
)

// authCodeGrantCLI implements the Authorization Code Grant (with PKCE).
//
// The client is not the owner of the resource.
//
// REF: https://pkg.go.dev/golang.org/x/oauth2#example-Config.
func authCodeGrantCLI() (err error) {
	const (
		challengeMethod string = "S256"
	)

	var (
		code      string
		respState string
	)

	clientState = generateRandString(StateLen)

	fmt.Println("Executing the Authorization Code Grant implementation")

	codeChallenge, err := generateCodeChallenge()
	if err != nil {
		return
	}

	// Specify the requirement for a refresh token & use PKCE.
	url := clientConf.AuthCodeURL(clientState, oauth2.AccessTypeOffline,
		oauth2.SetAuthURLParam("code_challenge", codeChallenge),
		oauth2.SetAuthURLParam("code_challenge_method", challengeMethod))
	fmt.Printf("Visit the URL for authentication: %s", url)

	// Read the state appended to the redirect URL from stdin.
	fmt.Printf("\nInput the `state`:")
	if _, err = fmt.Scan(&respState); err != nil {
		err = fmt.Errorf("unable to read the state: %w", err)
		return
	}
	if respState != clientState {
		err = fmt.Errorf("returned state is invalid")
		return
	}

	// Read the authorization code appended to the redirect URL from stdin.
	fmt.Printf("\nInput the authorization `code`:")
	if _, err = fmt.Scan(&code); err != nil {
		err = fmt.Errorf("unable to read the authorization code: %w", err)
		return
	}
	// Perform the handshake to retrieve an access token.
	token, err := clientConf.Exchange(ctx, code, oauth2.SetAuthURLParam("state", codeVerifier))
	if err != nil {
		err = fmt.Errorf("unable to convert authorization code to a token: %w", err)
		return
	}

	// The HTTP Client returned by conf.Client will refresh the token as necessary.
	content, err := clientAuthTest(clientConf.Client(ctx, token))
	if err != nil {
		return
	}
	fmt.Println("Response: ", content)

	return
}

// clientCredsGrantCLI implements the Client Credentials Grant.
//
// The client is the owner of the resource.  Can also used when requesting access to previously
// authorized resources.
//
// REF: https://pkg.go.dev/golang.org/x/oauth2/clientcredentials.
func clientCredsGrantCLI() (err error) {
	fmt.Println("Executing the Client Credentials Grant implementation")

	// The generated client will have requested an access token.
	content, err := clientAuthTest(resourceOwnerConf.Client(ctx))
	if err != nil {
		return
	}
	fmt.Println("Response: ", content)

	return
}

// resourceOwnerPassCredsGrantCLI implements the Resource Owner Password Credentials Grant.
//
// The client is the owner of the resource, using a username & password pair to authenticate.
//
// REF: https://pkg.go.dev/golang.org/x/oauth2/clientcredentials.
func resourceOwnerPassCredsGrantCLI() (err error) {
	fmt.Println("Executing the Resource Owner Password Credentials Grant implementation")

	token, err := clientConf.PasswordCredentialsToken(ctx,
		cfg.Secret.OwnerUsername, cfg.Secret.OwnerPassword)
	if err != nil {
		err = fmt.Errorf("unable to authenticate the user: %w", err)
		return
	}
	client := clientConf.Client(ctx, token)
	content, err := clientAuthTest(client)
	if err != nil {
		return
	}
	fmt.Println("Response: ", content)

	return
}

// authCodeGrantObtainAuthCode implements the authorization code request for the Authorization Code
// Grant (with PKCE).
//
// The client is not the owner of the resource.
//
// REF: https://pkg.go.dev/golang.org/x/oauth2#example-Config.
func authCodeGrantObtainAuthCode(wr http.ResponseWriter, req *http.Request) {
	const (
		challengeMethod string = "S256"
	)

	clientState = generateRandString(StateLen)

	codeChallenge, err := generateCodeChallenge()
	if err != nil {
		http.Error(wr, err.Error(), http.StatusInternalServerError)
		return
	}

	// Redirect user to consent page to ask for permission for the scopes specified above. Request a
	// refresh token at the same time.
	url := clientConf.AuthCodeURL(clientState, oauth2.AccessTypeOffline,
		oauth2.SetAuthURLParam("code_challenge", codeChallenge),
		oauth2.SetAuthURLParam("code_challenge_method", challengeMethod))

	http.Redirect(wr, req, url, http.StatusFound)
}

// authCodeGrantObtainToken implements the token request for the Authorization Code Grant (with
// PKCE).
//
// The the client is not the owner of the resource.
func authCodeGrantObtainToken(wr http.ResponseWriter, req *http.Request) {
	// Parse the URL query after redirection from the authorization server.
	if err := req.ParseForm(); err != nil {
		http.Error(wr, err.Error(), http.StatusBadRequest)
		return
	}

	respState := req.Form.Get("state")
	if respState != clientState {
		http.Error(wr, "Invalid state", http.StatusBadRequest)
		return
	}
	if respState != clientState {
		http.Error(wr, "returned state is invalid", http.StatusBadRequest)
		return
	}

	code := req.Form.Get("code")
	if code == "" {
		http.Error(wr, "Authorization code not found", http.StatusBadRequest)
		return
	}

	token, err := clientConf.Exchange(req.Context(), code,
		oauth2.SetAuthURLParam("code_verifier", codeVerifier))
	if err != nil {
		http.Error(wr, err.Error(), http.StatusInternalServerError)
		redirectTo(wr, "/")

		return
	}

	content, err := clientAuthTest(clientConf.Client(req.Context(), token))
	if err != nil {
		return
	}

	enc := json.NewEncoder(wr)
	enc.SetIndent("", "  ")
	_ = enc.Encode(content)
}

// loginHandler handles the "/login" route requests.
func loginHandler(wr http.ResponseWriter, req *http.Request) {
	if req.Method == "POST" {
		if req.Form == nil {
			if err := req.ParseForm(); err != nil {
				http.Error(wr, err.Error(), http.StatusBadRequest)
				return
			}
		}

		token, err := clientConf.PasswordCredentialsToken(req.Context(), req.Form.Get("username"), req.Form.Get("password"))
		if err != nil {
			http.Error(wr, err.Error(), http.StatusForbidden)
			return
		}

		content, err := clientAuthTest(clientConf.Client(req.Context(), token))
		if err != nil {
			return
		}

		enc := json.NewEncoder(wr)
		enc.SetIndent("", "  ")
		_ = enc.Encode(content)

		return
	}

	wr.Header().Set("Location", "/login")
	wr.WriteHeader(http.StatusFound)
}
